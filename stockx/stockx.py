import sys
import threading
import logging
import time
import bcrypt
import pymongo
import json
import yfinance as yf
import web



# db Client
myclient = pymongo.MongoClient("mongodb://localhost:27017/")
# Database
mydb = myclient["Accounts"]
# Colection
mycol = mydb["users"]


# - Logging
logging.basicConfig(level=logging.DEBUG, filename='stockx.log', filemode='a', format='[%(levelname)s] | %(message)s |Line: %(lineno)d |Time: %(asctime)s')
logging.info('Program StockX >> start')



# Tickers
msft = yf.Ticker("MSFT") ; apple = yf.Ticker("AAPL")



class acc:
    def __init__(self,username,balance,ticker,ammount):
        self.username = username
        self.ballance = balance
        self.ticker = ticker
        self.ammount = ammount
        self.price = ticker.info["regularMarketPrice"]

    def buy_stock(self): # Todo
        total_price = self.price * self.ammount
        total_price_with_charge = total_price + total_price / 100 * 7
        find_by_username = mycol.find_one({"username": self.username})
        balance = find_by_username.get("ballance")
        change = balance - total_price_with_charge
        stocks = find_by_username.get("stocks")
        find_balance_username = { "ballance": balance }
        new_balance_values = { "$set": { "ballance": change } }
        mycol.update_one(find_balance_username, new_balance_values)
        find_stock_username = { "stocks": stocks }
        newstock = stocks.append(str(self.ticker.info["longName"]))
        new_stocks_values = { "$set": { "stock": newstock } }
        mycol.update_one(find_stock_username, new_stocks_values)
    """
    def sell_stock(self):
        total_price = self.price * self.ammount
        find_by_username = mycol.find_one({"username": self.username})
        balance = find_by_username.get("ballance")
        stocks = find_by_username.get("stocks")
    """

acc("Emanuelus", 0, msft, 1).buy_stock()
#acc("erdogan", 12000, msft, 25).sell_stock()

# inside function
def hashing_password(password):
    logging.info('hashing-password')
    return bcrypt.hashpw(password.encode(), bcrypt.gensalt(rounds=10)).decode()



# inside function
def checking_password(password,password_in_db):
    logging.info('checking-password')
    return bcrypt.checkpw(password.encode(), password_in_db.encode())



# before login
def account_registration(username,password):
    logging.info('ACC - registration try - user: %s', username)
    if len(username) >= 8:
        registeredacc = mycol.find_one({"username": username})
        if registeredacc:
            logging.warning('%s already registered', username)
            print("Si zaregistrovaný")
        else:
            hashedpassword = hashing_password(password)
            entry_creation = { "username": username, "password": hashedpassword, "ballance": 0, "stocks":[]}
            mycol.insert_one(entry_creation)
            logging.info('new user registered: %s', username)
            print("Sup do db!")
    else:
        logging.warning('bad lenght of username for %s', username)
        print("Bad lenght of username")


# after login
def account_delete(username,password):
    logging.info('ACC - delete - try - user: %s', username)
    find_by_username = mycol.find_one({"username": username})
    if find_by_username:
        password_in_db = find_by_username.get("password")
        password_compare = checking_password(password,password_in_db)
        if password_compare:
            entry_delete = { "username": username }
            mycol.delete_one(entry_delete)
            print(f"User: {username} deleted")
            logging.info('%s deleted from accounts.', username)
        else:
            logging.warning('cant delete %s > bad password', username)
            print("Bad password")
    else:
        logging.warning('cant delete %s, not in accounts', username)
        print("No entry for this user")



# before login # Index
def account_login(username,password_one,password_two):
    if username and password_one and password_two:
        logging.info('ACC - login - try - user:%s', username)
        find_by_username = mycol.find_one({"username": username})
        if find_by_username and password_one == password_two:
            password = password_one or password_two
            password_in_db = find_by_username.get("password")
            password_compare = checking_password(password,password_in_db)
            if password_compare:
                logging.info('%s logged in', username)
                print("Logged-in")
            else:
                logging.warning('%s not logged in: bad password', username)
                print("Bad password")
        else:
            logging.warning('%s not logged in: passwords are not same or user is not found.')
            print("user not found or passwords are not same")
    else:
        print("emptyspace")

account_login("Emanuelus","Smetana","Smetana")
#account_registration("Emanuelus","Smetana")
#account_delete("Emanuelus","Smetana")
for x in mycol.find():
 print(x)



"""
def main():
    port = 5000
    run_web = threading.Thread(target=web.start, args=(port,))
    run_web.start()
    while True:
            try:
                time.sleep(1000)
            except KeyboardInterrupt:
                break
    web.stop()


if __name__ == "__main__":
    sys.exit(main())
"""